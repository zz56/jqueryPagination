<%@ page language="java" import="java.util.*"
    contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%  
String path = request.getContextPath();  
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";  
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>异步分页</title>
<!-- Bootstrap -->
<link href="./resource/css/bootstrap.min.css" rel="stylesheet" />
<link href="./resource/css/divpage.css" rel="stylesheet" type="text/css" />

<script src="http://code.jquery.com/jquery.js"></script>
<script src="./resource/js/bootstrap.min.js"></script>
<script type="text/javascript" src="./resource/js/asyn_page.js"></script>
<script type="text/javascript"> 
	 //var totalRowSize = ${totalRowSize}; 
	 var totalRowSize = 65;
	 $(document).ready(function(){ 
	  	$("#pageWidget").asynPage("http://localhost:8080/Tserver/findUser_asyn.action","#tbody",buildHtml,totalRowSize); 
	 }); 
	  
	 //构建内容 
	 function buildHtml(users){
	 var userList = JSON.parse(users);
	 $.each(userList,function(i,user){
	 	   var tr = [
	 	    '<tr>',
	 	    '<td>',user.id,'</td>',
	 	     '<td>',user.user,'</td>',
	 	     '<td>',user.password,'</td>',
	 	     '<td>',user.sex,'</td>',
	 	     '<td>',user.age,'</td>',
	 	     '<td>',user.email,'</td>',
	 	     '<td></td>',
	 	    '</tr>'
	 	   ].join(''); 
	 	   $("#tbody").append(tr); 
	  	});
	 }
	 </script>
</head>
<body>
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="page-header">
			<h1>
				异步分页 <small>I AM PAGE HEADER.</small>
			</h1>
		</div>

		<!-- Table -->
		<table class="table table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>用户名</th>
					<th>密码</th>
					<th>性别</th>
					<th>年龄</th>
					<th>Email</th>
				</tr>
			</thead>
			<tbody id="tbody" class="table-hover"></tbody>
		</table>
		<div id="pageWidget" class="page"></div>
	</div>
	<script src="<%=basePath %>/resource/js/bootstrap.min.js"></script>
</body>
</html>